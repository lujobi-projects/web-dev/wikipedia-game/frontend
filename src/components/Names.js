import React, { useEffect, useState, useRef } from 'react'
import { createMuiTheme, withStyles, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'

import DoneIcon from '@material-ui/icons/Done';


const NameButton = withStyles((theme) => ({
  root: {
    borderRadius: '50%'
  },
}))(Button);

export default function Names(props) {

  const [height, setHeight] = useState(0)
  const [width, setWidth] = useState(0)
  const [points, setPoints] = useState([])
  const ref = useRef(null)

  const poissonDiscSample = (width, height, cnt, points = []) => {

    const distance = (a, b) => {
      const dx = a[0] - b[0], dy = a[1] - b[1]
      return Math.sqrt(dx * dx + dy * dy);
    }

    for (let i = points.length; i < cnt; ++i) {
      let bestCand, bestDist = 0
      for (let j = 0; j < 20; ++j) {
        const cand = [Math.floor(Math.random() * width), Math.floor(Math.random() * height)]
        const minDist = points.reduce((min, elem) => Math.min(min, distance(elem, cand)), width + height)
        if (minDist > bestDist) {
          [bestDist, bestCand] = [minDist, cand]
        }
      }
      points.push(bestCand)
    }

    return points
  }

  useEffect(() => {
    console.log(ref)
    setHeight(ref.current.clientHeight)
    setWidth(ref.current.clientWidth)
    if (points.length != props.names.length && height > 0) {
      setPoints(poissonDiscSample(width * 0.8, height * 0.7, props.names.length, [...points]))
    }
  })

  return (
    <Box my={1} flexGrow={1} position="relative" ref={ref}>
      {points.map(([x, y], ind) => (
        <NameButton key={ind}
          variant="contained"
          color={props.names[ind] === props.selected ? "secondary" : "primary"}
          size="large"
          disabled={props?.disabled === true || props?.disabled?.includes(props.names[ind])}
          onClick={props.handleClick ? () => props.handleClick(props.names[ind]) : null}
          style={{
            position: 'absolute',
            left: x + 0.1 * width,
            top: y + 0.15 * height
          }}>
          <div>{props.names[ind]}</div> {props?.voted?.includes(props.names[ind]) && <DoneIcon />}
        </NameButton>
      ))}
    </Box>
  )

}