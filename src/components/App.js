import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { sizing } from '@material-ui/system';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import ChooseName from '../features/game/ChooseName';
import ChooseGame from '../features/game/ChooseGame';
import { useSelector } from 'react-redux';
import { GameState } from '../features/game/gameSlice';
import StartGame from '../features/game/StartGame';
import ChooseArticle from '../features/game/ChooseArticle';
import { Vote, Finished } from '../features/game';
import Names from './Names';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function App() {

  const name = useSelector(state => state.game.name)
  const gameState = useSelector(state => state.game.game)
  const state = useSelector(state => state)

  return (
    <Container maxWidth="sm">
      <Box py={4} height="100vh" display="flex" flexDirection="column">
        <Typography variant="h4" component="h1" align="center">
          Wikipedia-Game
        </Typography>
        {name == undefined && <ChooseName />}
        {name && gameState === GameState.NONE && <ChooseGame />}
        {gameState === GameState.WAITING && <StartGame />}
        {gameState === GameState.SEARCHING && <ChooseArticle />}
        {gameState === GameState.VOTING && <Vote />}
        {gameState === GameState.FINISHED && <Finished />}
        {/* <Names names={["Ian", "Elias"]}/> */}
        {/* <StartGame /> */}
        {/* <Copyright /> */}
      </Box>

      {/* <Typography variant="h4" component="h1" align="center">
        Wikipedia-Game {JSON.stringify(state)}
      </Typography> */}
    </Container>
  );
}
