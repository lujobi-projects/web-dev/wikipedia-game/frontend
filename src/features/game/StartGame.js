import React, { useContext } from 'react';
import Button from '@material-ui/core/Button';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from 'react-redux';
import { WebSocketContext } from '../../WebSocket';
import { createGame, startGame } from './gameSlice';
import Names from '../../components/Names';


export default function StartGame() {

  const ws = useContext(WebSocketContext);
  const name = useSelector(state => state.game.name)
  const pin = useSelector(state => state.game.pin)
  const players = useSelector(state => state.game.players)
  const dispatch = useDispatch()

  console.log(players)

  return (<>
    <Grid container justify="center" spacing={2}>
      <Grid item sm={8} xs={12}>
        <Typography variant="h6" component="h1" align="center">
          Waiting for players to join.
          <br />
          Game-PIN: {pin}
        </Typography>
      </Grid>
    </Grid>
    <Names names={players} disabled />
    <Grid container justify="center" spacing={2}>
      <Grid item sm={8} xs={12}>
        <Button fullWidth variant="contained" size="large" color="primary" onClick={() => dispatch(startGame(ws))}>
          Start Game
        </Button>
      </Grid>
    </Grid>
  </>)
}