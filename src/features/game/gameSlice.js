import { createSlice } from '@reduxjs/toolkit'
import { useContext } from 'react';
import { getRandomArticleUrl } from '../../api/wikiapi';
import { WebSocketContext } from '../../WebSocket';

export const GameState = {
    NONE: 'NONE',
    WAITING: 'WAITING',
    SEARCHING: 'SEARCHING',
    VOTING: 'VOTING',
    FINISHED: 'FINISHED'
}

const gameSlice = createSlice({
    name: 'game',
    initialState: { game: GameState.NONE, name: null, players: [] },
    reducers: {
        chooseName: (state, action) => ({ ...state, name: action.payload.name }),
        joinGameSuccess: (state, action) => ({ ...state, game: GameState.WAITING, ...action.payload }),
        startGameSuccess: (state) => ({ ...state, game: GameState.SEARCHING }),
        chooseArticleSuccess: (state, { payload }) => ({ ...state, game: payload.title ? GameState.VOTING : state.game, ...payload }),
        voteSuccess: (state, { payload }) => ({ ...state, ...payload, game: payload?.progress === -1 ? GameState.FINISHED : GameState.VOTING })
    }
})

export const { chooseName, joinGameSuccess, startGameSuccess, chooseArticleSuccess, voteSuccess } = gameSlice.actions

export const createGame = (ws, name) => async dispatch => {
    ws.request({ type: 'CREATE_GAME', 'name': name }, 'CREATE_GAME').then(
        ({ data }) => dispatch(joinGameSuccess(data)),
        err => console.log(err)
    )
}

export const joinGame = (ws, name, pin) => async dispatch => {
    ws.request({ type: 'JOIN_GAME', name: name, pin: pin }, 'JOIN_GAME').then(
        ({ data }) => dispatch(joinGameSuccess(data)),
        err => console.log(err)
    )
}

export const startGame = (ws) => async dispatch => {
    ws.send({ type: 'START_GAME' })
}

export const chooseArticle = (ws, title) => async dispatch => {
    ws.request({ type: 'CHOOSE_ARTICLE', article: title }, 'CHOOSE_ARTICLE').then(
        ({ data }) => dispatch(chooseArticleSuccess(data)),
        err => console.log(err)
    )
}

export const vote = (ws, name) => async dispatch => {
    ws.request({ type: 'VOTE', name }, 'VOTE').then(
        () => {},
        err => console.log(err)
    )
}



export default gameSlice.reducer