import React, { useContext, useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';

import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Box from '@material-ui/core/Box'
import { useDispatch } from 'react-redux';
import { chooseArticle, chooseName, newArticle } from './gameSlice';
import { WebSocketContext } from '../../WebSocket';
import { getRandomArticleUrl } from '../../api/wikiapi';


const Alert = (props: AlertProps) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function ChooseArticle() {

  const ws = useContext(WebSocketContext);
  const dispatch = useDispatch()
  const [article, setArticle] = useState('')
  const [title, setTitle] = useState('')


  const [open, setOpen] = useState(false);
  const [ready, setReady] = useState(false);
  const [showSnackbar, setShowSnackbar] = useState(false);


  const handleCloseSnackbar = () => {
    setShowSnackbar(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const newArticle = () => {
    getRandomArticleUrl().then(
      data => {
        const url = ((Object.values(data.query.pages)[0]).fullurl).replace("de.wikipedia", "de.m.wikipedia")
        const title = (Object.values(data.query.pages)[0]).title
        setArticle(url)
        setTitle(title)
      },
      err => console.log(err)
    )
  }

  useEffect(() => {
    if (article === '') {
      newArticle()
    }
  })

  const adjustHeight = target => {
    target.style.height = target.contentWindow.document.body.scrollHeight + 'px';
  }

  return (<>
    <Button disabled={ready} fullWidth variant="contained" size="large" color="primary" onClick={() => newArticle()}>
      New Article
    </Button>
    <Box my={1} flexGrow={1}>
      <iframe width="100%" height="100%" src={article} onLoad={(e) => console.log(e)}></iframe>
    </Box>
    <Button disabled={ready} fullWidth variant="contained" size="large" color="primary" onClick={handleClickOpen}>
      Take it
    </Button>

    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Are you ready to present?</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Read the article carefully!
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Read again
        </Button>
        <Button onClick={() => { dispatch(chooseArticle(ws, title)); setReady(true); setShowSnackbar(true); handleClose() }} color="primary" autoFocus>
          Ready
        </Button>
      </DialogActions>
    </Dialog>
    <Snackbar open={showSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
      <Alert onClose={handleCloseSnackbar} severity="info">
        Waiting for other players
      </Alert>
    </Snackbar>
  </>
  )
}