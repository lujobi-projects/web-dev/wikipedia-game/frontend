import React, { useContext, useState } from 'react';
import Button from '@material-ui/core/Button';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from 'react-redux';
import { WebSocketContext } from '../../WebSocket';
import { createGame, joinGame } from './gameSlice';


export default function ChooseGame() {

  const ws = useContext(WebSocketContext);
  const name = useSelector(state => state.game.name)
  const [pin, setPin] = useState('')
  const dispatch = useDispatch()

  return (
    <Grid container justify="center" spacing={2}>
      <Grid item sm={8} xs={12}>
        <Typography variant="h6" component="h1" align="center">
          Enter Game PIN
        </Typography>
      </Grid>
      <Grid item sm={8} xs={12}>
        <FormControl fullWidth variant="outlined">
          <InputLabel htmlFor="outlined-adornment-gamepin">Gamepin</InputLabel>
          <OutlinedInput
            id="outlined-adornment-gamepin"
            onChange={e => setPin(e.target.value)}
            onKeyDown={e => { if (e.key == "Enter") { dispatch(joinGame(ws, name, pin)) } }}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  variant="contained"
                  aria-label="join game"
                  edge="end"
                  onClick={() => dispatch(joinGame(ws, name, pin))}
                >
                  <ArrowForwardRoundedIcon />
                </IconButton>
              </InputAdornment>
            }
            labelWidth={70}
          />
        </FormControl>
      </Grid>
      <Grid item sm={8} xs={12}>
        <Button fullWidth variant="contained" size="large" color="primary" disabled={pin} onClick={() => dispatch(createGame(ws, name))}>
          Create Game
        </Button>
      </Grid>
    </Grid>)
}