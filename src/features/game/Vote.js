import React, { useContext, useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from 'react-redux';
import { WebSocketContext } from '../../WebSocket';
import { createGame, startGame, vote } from './gameSlice';
import Names from '../../components/Names';


export default function Vote() {

  const ws = useContext(WebSocketContext);
  const name = useSelector(state => state.game.name)
  const players = useSelector(state => state.game.players)
  const voted = useSelector(state => state.game.progress)
  const title = useSelector(state => state.game.title)
  const dispatch = useDispatch()

  console.log(players)
  const [selection, setSelection] = useState("")
  const [done, setDone] = useState(false)
  const [currentTitle, setCurrentTitle] = useState(title);

  useEffect(() => {
    if (currentTitle === title) {
      return
    }
    setDone(false)
    setSelection("")
    setCurrentTitle(title)
  }, [title])

  const voteFct = () => {
    setDone(true)
    dispatch(vote(ws, selection))
  }

  return (
    <>
      <Grid container justify="center" spacing={2}>
        <Grid item sm={8} xs={12}>
          <Typography variant="h6" component="h1" align="center">
            Explaining / Voting
          </Typography>
          <Typography variant="h5" component="h1" align="center">
            {title}
          </Typography>
        </Grid>
      </Grid>

      <Names names={players} disabled={done || name} voted={voted} selected={selection} handleClick={(name) => setSelection(name)} />

      <Button disabled={done} fullWidth variant="contained" size="large" color="primary" onClick={() => voteFct()}>
        Vote
      </Button>
    </>
  )
}