import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import { useDispatch } from 'react-redux';
import { chooseName } from './gameSlice';


export default function ChooseName() {

  const dispatch = useDispatch()
  const [name, setName] = useState('')

  return (

    <Grid container justify="center" spacing={2}>

      <Grid item sm={8} xs={12}>
        <Typography variant="h6" component="h1" align="center">
          Choose your Name
        </Typography>
      </Grid>
      <Grid item sm={8} xs={12}>
        <FormControl fullWidth variant="outlined">
          <InputLabel htmlFor="outlined-adornment-name">Name</InputLabel>
          <OutlinedInput
            onInput={e => setName(e.target.value)}
            onKeyDown={e => { if (e.key == "Enter") { dispatch(chooseName({ name: name })) } }}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  variant="contained"
                  aria-label="enter name"
                  edge="end"
                  onClick={() => dispatch(chooseName({ name: name }))}
                >
                  <ArrowForwardRoundedIcon />
                </IconButton>
              </InputAdornment>
            }
            labelWidth={70}
          />
        </FormControl>
      </Grid>
    </Grid>)
}