import React, { useContext } from 'react';
import Button from '@material-ui/core/Button';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from 'react-redux';
import { WebSocketContext } from '../../WebSocket';
import { createGame, startGame, vote } from './gameSlice';
import Names from '../../components/Names';


import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


export default function Vote() {

  const ws = useContext(WebSocketContext);
  const name = useSelector(state => state.game.name)
  const results = useSelector(state => state.game.results)
  const players = useSelector(state => state.game.players)

  let table = {}

  const res = JSON.parse(JSON.stringify(results))

  res.sort((a, b) => a.score - b.score)

  res.forEach(e => {
    if (table["" + e.score]) {
      table["" + e.score].push(e.name)
    } else {
      table["" + e.score] = [e.name]
    }
  })


  return (<>
    <Grid container justify="center" spacing={2}>

      <Grid item sm={8} xs={12}>
        <Typography variant="h6" component="h1" align="center">
          Finished
        </Typography>
      </Grid>
    </Grid>
    <TableContainer component={Paper}>
      <Table aria-label="spanning table">
        <TableHead>
          <TableRow>
            <TableCell>Score</TableCell>
            <TableCell>Player</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {Object.keys(table).reverse().map(key => (
            <TableRow key={key}>
              <TableCell>{key}</TableCell>
              <TableCell align="right">
                <Table aria-label="spanning table" component={Paper}>
                  <TableBody>
                    {table[key].map(n => (
                      <TableRow key={n}>
                        <TableCell>{n}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>

    <Button fullWidth variant="contained" size="large" color="primary" onClick={() => window.location.reload()}>
      Restart
    </Button>
  </>
  )
}