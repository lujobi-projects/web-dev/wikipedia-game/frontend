import axios from 'axios'
import { WIKIPEDIA_PROTOCOL } from '../config'


export async function getRandomArticleUrl() {
  const url = `https://de.wikipedia.org/w/api.php?action=query&origin=*&format=json&prop=info&generator=random&inprop=url&grnnamespace=0`

  const { data } = await axios.get(url)
  return data
}
