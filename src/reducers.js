import { combineReducers } from 'redux'
import gameReducer from './features/game/gameSlice'

export default combineReducers({
  game: gameReducer,
})