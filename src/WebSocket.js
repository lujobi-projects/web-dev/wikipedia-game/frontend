import React, { createContext } from 'react'
import { WS_BASE } from './config';
import { useDispatch } from 'react-redux';
import { startGameSuccess, joinGameSuccess, chooseArticleSuccess, voteSuccess } from './features/game/gameSlice';

const WebSocketContext = createContext(null)

export { WebSocketContext }

export default ({ children }) => {
    let socket
    let ws

    const dispatch = useDispatch()

    const send = (message) => socket.send(JSON.stringify(message))

    const request = (message, answerType) => {
        socket.send(JSON.stringify(message))

        return new Promise((resolve, reject) => {
            const handleMessage = ({data}) => {
                console.log(data)
                let msg = JSON.parse(data)
                if(msg.type == answerType) {
                    socket.removeEventListener('message', handleMessage)
                    if(msg.status == "SUCCESS") {
                        resolve(msg)
                    } else{
                        reject(msg.status)
                    }
                }
                else{
                    reject("wrong message type", msg)
                }
            }

            socket.addEventListener('message', handleMessage)
        })
    }

    if (!socket) {
        socket = new WebSocket(WS_BASE)

        socket.onopen = () => {
            console.log("Websocket connected")
        }

        socket.addEventListener('message', ({ data }) => {
            const msg = JSON.parse(data)
            console.log(msg)
            const dispatchActions = {
                'START_GAME': () => startGameSuccess(),
                'JOIN_GAME': ({data}) => joinGameSuccess(data),
                'CHOOSE_ARTICLE': ({data}) => chooseArticleSuccess(data),
                'VOTE': ({data}) => voteSuccess(data)
            }
            if(dispatchActions[msg.type] && msg.status == "BROADCAST") {
                dispatch(dispatchActions[msg.type](msg))
            }
        })

        ws = {
            socket: socket,
            request,
            send

        }
    }

    return (
        <WebSocketContext.Provider value={ws}>
            {children}
        </WebSocketContext.Provider>
    )
}